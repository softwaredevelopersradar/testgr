﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GrozaS_AWS
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public SizeValue sizeDPSetting { get; set; }
        public SizeValue sizeDPTables { get; set; }
        public SizeValue sizeDPJamming { get; set; }
        public SizeValue sizeDPSpoofing { get; set; }


        public StateConnection stateConnectionOEM { get; set; }

        public StateConnection stateConnectionDB { get; set; }

        public StateConnection stateConnectionDF { get; set; }







        public void SetDefaultDPanel()
        {
            sizeDPSetting.SetDefault();
            sizeDPTables.SetDefault();
            sizeDPJamming.SetDefault();
            sizeDPSpoofing.SetDefault();
        }

        public MainWindowViewModel()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");

            sizeDPSetting = new SizeValue(DefaultSize.WidthDPanelSetting);
            sizeDPTables = new SizeValue(DefaultSize.HeightDPanelJammer);
            sizeDPJamming = new SizeValue(DefaultSize.WidthDPanelJamming);
            sizeDPSpoofing = new SizeValue(DefaultSize.WidthDPanelSpoofing);

            stateConnectionOEM = new StateConnection();
            stateConnectionDB = new StateConnection();
            stateConnectionDF = new StateConnection();

        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}
