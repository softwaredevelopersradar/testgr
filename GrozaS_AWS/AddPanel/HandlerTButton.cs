﻿
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;



namespace GrozaS_AWS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        private void DefaultPanelToggleButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindowViewModel.SetDefaultDPanel();
        }

    }
}