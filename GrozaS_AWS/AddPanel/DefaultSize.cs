﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS
{
    
    class DefaultSize
    {
        public const double HeightDPanelJammer = 100;

        public const double WidthDPanelSetting = 300;

        public const double WidthDPanelJamming = 90;

        public const double WidthDPanelSpoofing = 90;


    }
}
