﻿
using DllGrozaSProperties.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using YamlDotNet.Serialization;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {

        LocalProperties localProperties;
        LocalProperties defaultLocalProperties;

        public void InitLocalProperties()
        {
            defaultLocalProperties = YamlLoad(AppDomain.CurrentDomain.BaseDirectory + "DefaultLocalProperties.yaml");

            localProperties = YamlLoad(AppDomain.CurrentDomain.BaseDirectory + "LocalProperties.yaml");

            SetLocalProperties();
        }

        public void SetLocalProperties()
        {
            try
            {
                basicProperties.Local = localProperties;
            }
            catch { }
        }


        public void HandlerLocalProperties(object sender, LocalProperties arg)
        {
            
            YamlSave(arg);

            localProperties = arg;

        }

        public void HandlerGlobalProperties(object sender, GlobalProperties arg)
        {
            //clientDB?.Tables[NameTable.GlobalProperties].Add(arg);
            
        }


        private void HandlerLocalDefaultProperties_Click(object sender, EventArgs e)
        {
            try
            {
                
                HandlerLocalProperties(this, defaultLocalProperties);

                SetLocalProperties();
            }
            catch
            { }
        }
    }
}