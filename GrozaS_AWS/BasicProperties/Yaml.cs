﻿using DllGrozaSProperties.Models;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        public LocalProperties YamlLoad(string PathFile)
        {
            string text = "";
            try
            {                
                using (StreamReader sr = new StreamReader(PathFile, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var localProperties = new LocalProperties();
            try
            {
                localProperties = deserializer.Deserialize<LocalProperties>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (localProperties == null)
            {
                localProperties = GenerateDefaultLocalProperties();
                YamlSave(localProperties);
            }
            return localProperties;
        }

        private LocalProperties GenerateDefaultLocalProperties()
        {

            

             var localProperties = new LocalProperties();
            
            localProperties.DB.IpAddress = "192.168.100.116";
            localProperties.DB.Port = 8302;

            localProperties.DF.IpAddress = "192.168.100.116";
            localProperties.DF.Port = 10001;

            localProperties.ED.IpAddress = "192.168.100.116";
            localProperties.ED.Port = 10001;

            localProperties.Lemt.IpAddressLocal = "192.168.100.116";
            localProperties.Lemt.IpAddressRemoute = "192.168.100.238";
            localProperties.Lemt.PortLocal = 1281;
            localProperties.Lemt.PortRemoute = 1283;

            localProperties.Rodnik.IpAddressLocal = "192.168.100.116";
            localProperties.Rodnik.IpAddressRemoute = "192.168.0.77";
            localProperties.Rodnik.PortLocal = 44444;
            localProperties.Rodnik.PortRemoute = 44444;

            localProperties.OEM.IpAddressLocal = "192.168.100.116";
            localProperties.OEM.IpAddressRemoute = "192.168.100.238";
            localProperties.OEM.PortLocal = 5553;
            localProperties.OEM.PortRemoute = 5553;

            return localProperties;
        }

        public T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //if (t == null)
            //{
            //    t = new T();
            //    YamlSave(t, NameDotYaml);
            //}
            return t;
        }

        public void YamlSave(LocalProperties localProperties)
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(localProperties);

                using (StreamWriter sw = new StreamWriter("LocalProperties.yaml", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
